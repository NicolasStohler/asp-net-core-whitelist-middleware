# ASP.NET Core 2 - Middleware Demo

This demonstrates how to create a middleware class that does a simple "basic authorization" header check for an allowed username/passord combination. If that fails, 401 (Unauthorized) is returned.

This is basically a mash up of the following sources:
- https://damienbod.com/2016/12/18/implementing-a-client-white-list-using-asp-net-core-middleware/
  > - AdminWhiteListMiddleware
- https://blog.dangl.me/archive/http-basic-authentication-in-aspnet-core-projects/
  > - BasicAuthenticationHeaderValue
- http://www.intstrings.com/ramivemula/articles/middleware-filters-in-asp-net-core/
  > - MiddlewareFilter  
  > - CustomPipeline

More middleware links:
- https://www.devtrends.co.uk/blog/conditional-middleware-based-on-request-in-asp.net-core


Encoding/Angular : 
- https://stackoverflow.com/a/45006503/54159
- https://stackoverflow.com/questions/45470575/angular-4-httpclient-query-parameters
- https://stackoverflow.com/questions/14627399/setting-authorization-header-of-httpclient
- https://stackoverflow.com/questions/11743160/how-do-i-encode-and-decode-a-base64-string
- https://stackoverflow.com/questions/10077237/httpclient-authentication-header-not-getting-sent
- https://stackoverflow.com/questions/41972330/base-64-encode-and-decode-a-string-in-angular-2
- https://stackoverflow.com/a/41505665/54159
- https://angular.io/api/common/http/HttpHeaders
﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace WhiteListMiddlewareDemo.Controllers
{
    [Route("api/[controller]")]
    public class DemoController : Controller
    {
        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {            
            return new string[] { "value1xxx", "value2yyy" };
        }
    }
}
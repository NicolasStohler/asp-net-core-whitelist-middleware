﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WhiteListMiddlewareDemo.Filters;

namespace WhiteListMiddlewareDemo.Controllers
{
    //[ServiceFilter(typeof(ClientIdCheckFilter))]
    //[MiddlewareFilter(typeof(AdminWhiteListMiddleware))]

    // works:
    //[MiddlewareFilter(typeof(CustomPipeline))]
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        private ILogger<ValuesController> _logger;

        public ValuesController(ILogger<ValuesController> logger)
        {
            _logger = logger;
        }

        // GET api/values
        [HttpGet]
        [MiddlewareFilter(typeof(CustomPipeline))]
        public IEnumerable<string> Get()
        {
            _logger.LogDebug("successful get.");
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}

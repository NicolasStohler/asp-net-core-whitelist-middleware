﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;

namespace WhiteListMiddlewareDemo
{
    public class CustomPipeline
    {
        public void Configure(IApplicationBuilder app, IConfiguration configuration)
        {
            app.UseMiddleware<AdminWhiteListMiddleware>(configuration["AdminWhiteList"]);
        }
    }
}
